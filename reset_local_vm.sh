VGNAME='vg_testlap'
LVSOURCE='lv-taskbot-base'
LVNAME='lv-taskbot-master'
VMNAME='taskbot-master'
MACADDR='52:54:00:1e:d2:cd'

# kill the vm
virsh destroy $VMNAME

# destroy the vm
virsh undefine $VMNAME

# remove the snapshot
lvremove --force /dev/$VGNAME/$LVNAME

# create new snapshot
lvcreate -s -L 50G -n $LVNAME /dev/$VGNAME/$LVSOURCE

# create new virtual machine
virt-install --import --name=$VMNAME --hvm --graphics spice --video qxl --arch=x86_64 --vcpus=2 --ram=1024 \
--os-type=linux --os-variant=Fedora18 --network network=default,mac=$MACADDR \
--disk path=/dev/$VGNAME/$LVNAME,device=disk,bus=virtio --noreboot

# start virtual machine
virsh start $VMNAME
