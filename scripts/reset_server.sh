LVNAME='f17server'
LVSNAPNAME='f17server_snap1'
VGNAME='vg_storage'
SNAPPOSTFIX='snap1'
SNAPSIZE='20G'
VMNAME='f17server_snap1'
SLEEPTIME='20'

# VM Settings
VMRAM=2048
VMCPU=2
VMMAC='52:54:00:3e:87:b4'

echo "== stopping virtual machine =="
virsh destroy "$VMNAME"
sleep "$SLEEPTIME"

echo ""
echo "== undefining virtual machine =="
virsh undefine "$VMNAME"

echo ""
echo "== removing LV =="
lvremove -f "/dev/$VGNAME/$LVSNAPNAME"

echo ""
echo "== taking snapshot =="
lvcreate -n "$LVSNAPNAME" -L 20G -s "/dev/$VGNAME/$LVNAME"

echo ""
echo "== re-creating virtual machine =="
virt-install --import --name=$VMNAME --hvm --graphics spice \
--video=qxl --arch=x86_64 --vcpus=$VMCPU --ram=$VMRAM \
--os-type=linux --os-variant=fedora17 \
--disk path=/dev/$VGNAME/$LVSNAPNAME,device=disk,bus=virtio \
--network network=default,mac="$VMMAC" --noreboot

echo ""
echo "== starting virtual machine =="
virsh start $VMNAME
